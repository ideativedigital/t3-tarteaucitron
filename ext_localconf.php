<?php

defined('TYPO3_MODE') or die("Access denied");

(function() {
    $xClasses = &$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'];
    $namespace = "TYPO3\\CMS\\Core\\Resource\\Rendering";

    $xClasses["$namespace\\YouTubeRenderer"] = ['className' => \Ideative\Tarteaucitron\Xclasses\TYPO3\CMS\Core\Resource\Rendering\YouTubeRenderer::class];
    $xClasses["$namespace\\VimeoRenderer"] = ['className' => \Ideative\Tarteaucitron\Xclasses\TYPO3\CMS\Core\Resource\Rendering\VimeoRenderer::class];
})();

