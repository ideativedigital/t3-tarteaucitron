
# TYPO3 Extension ``tarteaucitron``

TYPO3 wrapper for the french Tarteaucitron script.

[![Monthly Downloads](https://poser.pugx.org/ideative/t3-tarteaucitron/d/monthly?format=flat)](https://packagist.org/packages/ideativedigital/tarteaucitron)
[![License](https://poser.pugx.org/ideative/t3-tarteaucitron/license?format=flat)](https://packagist.org/packages/ideativedigital/tarteaucitron)

## 1. Features

- Based on extbase, implementing best practices from TYPO3 CMS
- Builds the Tarteaucitron loader script from TypoScript
- Allows customization through constants


## 2. Usage

### 1) Installation

#### Installation using Composer

The recommended way to install the extension is by using [Composer][1]. In your Composer based TYPO3 project root, just execute the following command 

    composer require ideative/t3-tarteaucitron 

#### Installation as extension from TYPO3 Extension Repository (TER)

Download and install the extension with the extension manager module.

### 2) Minimal setup

1) Include the static TypoScript of the extension. 
2) Customize the `constants.typoscript` to your needs.

## 3. Contributing

### 3.1. Pull requests

**Pull requests** are welcome! Nevertheless please be aware we might not read **PR**s very often and that we use this extension in a lot of projects, so we might not be able to adapt them all for a new feature. 

If you still want to submit a **PR**, don't forget to add an issue and connect it to your pull requests. This is very helpful to understand what kind of issue the **PR** is going to solve.

- Bugfixes: Please describe what kind of bug your fix solve and give us feedback how to reproduce the issue. We're going to accept only bugfixes if we can reproduce the issue.
- Features: Not every feature is relevant for us. In addition: We don't want to make the extension more complicated for an edge case feature. 

[1]: https://getcomposer.org/
